//
//  ANAppDelegate.h
//  AppNavigator
//
//  Created by huangruxue on 09/21/2018.
//  Copyright (c) 2018 huangruxue. All rights reserved.
//

@import UIKit;

@interface ANAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

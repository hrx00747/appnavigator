//
//  ANAppDelegate.m
//  AppNavigator
//
//  Created by huangruxue on 09/21/2018.
//  Copyright (c) 2018 huangruxue. All rights reserved.
//

#import "ANAppDelegate.h"

#import "ANNavigator.h"
#import "ANTabbarItem.h"
#import "ANNavigationBarView.h"
#import "ANpageAnimatorPushMenuLeft.h"
#import "ANpageAnimatorPopMenuLeft.h"
#import "Account.h"

@implementation ANAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    
    [ANNavigator registerSchemes:@[@"app"]];
    
    ANPageAnimatorPushMenuLeft *pushMenu = [[ANPageAnimatorPushMenuLeft alloc] init];
    ANPageAnimatorPopMenuLeft *popMenu = [[ANPageAnimatorPopMenuLeft alloc] init];
    [ANNavigator registerPageAnimation:pushMenu forKey:@"pushmenu"];
    [ANNavigator registerPageAnimation:popMenu forKey:@"popmenu"];

    [ANNavigationBarView appearance].titleColor = [UIColor whiteColor];
    [ANNavigationBarView appearance].textItemColor = [UIColor whiteColor];
    [ANNavigationBarView appearance].textItemFont = [UIFont systemFontOfSize:14];
    [ANNavigationBarView appearance].backgroundColor = [UIColor redColor];

    [self pageInit];
    return YES;
}

- (void) pageInit {
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor darkGrayColor],
                                 NSFontAttributeName:[UIFont systemFontOfSize:10]
                                 };
    NSDictionary *selectedAttributes = @{NSForegroundColorAttributeName:[UIColor redColor],
                                         NSFontAttributeName:[UIFont systemFontOfSize:10]
                                         };
    
    ANTabbarItem *homeItem = [[ANTabbarItem alloc] init];
    homeItem.url = @"app://NFFirstPage?@animation=none&@cache=true&title=Home&@navigationbar=true";
    homeItem.title = @"Home";
    homeItem.icon = @"tab_home";
    homeItem.selectedIcon = @"tab_home_highlight";
    homeItem.attributes = attributes;
    homeItem.selectedAttributes = selectedAttributes;

    ANTabbarItem *cartItem = [[ANTabbarItem alloc] init];
    cartItem.url = @"app://NFSecondPage?@animation=none&@cache=true&title=Cart";
    cartItem.title = @"Cart";
    cartItem.icon = @"tab_cart";
    cartItem.selectedIcon = @"tab_cart_highlight";
    cartItem.attributes = attributes;
    cartItem.selectedAttributes = selectedAttributes;

    ANTabbarItem *settingItem = [[ANTabbarItem alloc] init];
    settingItem.url = @"app://NFThirdPage?@animation=none&@cache=true&title=Setting";
    settingItem.title = @"Setting";
    settingItem.icon = @"tab_setting";
    settingItem.selectedIcon = @"tab_setting_highlight";
    settingItem.attributes = attributes;
    settingItem.selectedAttributes = selectedAttributes;

    NSArray *items = @[homeItem,cartItem,settingItem];
    [[ANNavigator main] forwardTo:@"app://MenuViewController?@animation=none&@cache=true&@navigationbar=false"];
    [[ANNavigator main] forwardWithItems:items params:@{@"@animation":@"none"} callback:nil];
    
    NSDictionary *params = @{@"url":@"app://LoginViewController",
                             @"@navigationbar":@1,
                             @"@animation":@"pushtop",
                             @"title":@"Login",
                             @"navigationBackImage":@"checkout_close_icon",
                             @"navigationBackAnimation":@"popbottom"
                             };
    ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
    [[ANNavigator main] setPreconditionTarget:target];
    [[ANNavigator main] setPreconditionBlock:^BOOL{
        return ([Account shared].sessionId.length == 0);
    }];
//    [[ANNavigator main] setPreconditionPages:@[@"NFSecondPage"]];
    
    self.window.rootViewController = [ANNavigator main];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

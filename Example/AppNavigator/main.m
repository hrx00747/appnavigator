//
//  main.m
//  AppNavigator
//
//  Created by huangruxue on 09/21/2018.
//  Copyright (c) 2018 huangruxue. All rights reserved.
//

@import UIKit;
#import "ANAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ANAppDelegate class]));
    }
}

//
//  LoginViewController.m
//  AppNavigator_Example
//
//  Created by chris huang on 2019/3/19.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "LoginViewController.h"
#import "Account.h"
#import "ANNavigator.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)onButtonPressed:(id)sender{
    
    [Account shared].sessionId = @"xxxx";
    [self.navigator backward:@{@"success":@YES,
                               @"@animation":@"popbottom"
                               }];
}

@end

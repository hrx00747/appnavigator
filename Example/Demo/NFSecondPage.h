//
//  NFSecondPage.h
//  NavigatorFramework
//
//  Created by chris on 16/1/2.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "ANPage.h"
#import "ANNavigator.h"

@interface Address : NSObject

@property(nonatomic, copy)  NSString        *country;
@property(nonatomic, copy)  NSString        *province;
@property(nonatomic, copy)  NSString        *city;

@end

@interface NFSecondPage : ANPage

@property(nonatomic, copy)      NSString        *name;
@property(nonatomic, assign)    NSInteger       age;
@property(nonatomic, strong)    Address         *myAddress;

@end

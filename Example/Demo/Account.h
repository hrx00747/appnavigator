//
//  Account.h
//  AppNavigator_Example
//
//  Created by chris huang on 2019/3/19.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Account : NSObject

@property(nonatomic, copy)  NSString        *sessionId;

+ (instancetype) shared;

@end

NS_ASSUME_NONNULL_END

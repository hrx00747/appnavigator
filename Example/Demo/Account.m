//
//  Account.m
//  AppNavigator_Example
//
//  Created by chris huang on 2019/3/19.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "Account.h"

@implementation Account

+ (instancetype) shared{
    static Account *instance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        instance = [[Account alloc] init];
    });
    return instance;
}

@end

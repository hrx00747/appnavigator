//
//  MenuViewController.h
//  AppNavigator_Example
//
//  Created by chris huang on 2019/2/26.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANPage.h"

NS_ASSUME_NONNULL_BEGIN

@interface MenuViewController :ANPage

@end

NS_ASSUME_NONNULL_END

//
//  TestViewController.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/20.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController ()

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor orangeColor];
}



@end

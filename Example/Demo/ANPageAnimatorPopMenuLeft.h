//
//  ANPageAnimatorPopMenuLeft.h
//  AppNavigator
//
//  Created by chris huang on 2018/12/28.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import "ANPageAnimator.h"
#import "ANPageAnimation.h"


@interface ANPageAnimatorPopMenuLeft : ANPageAnimator<ANPageAnimation>

@property(nonatomic, assign)    CGFloat         overlapWidth;

@end

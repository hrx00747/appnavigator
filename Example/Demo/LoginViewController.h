//
//  LoginViewController.h
//  AppNavigator_Example
//
//  Created by chris huang on 2019/3/19.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANPage.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : ANPage

@end

NS_ASSUME_NONNULL_END

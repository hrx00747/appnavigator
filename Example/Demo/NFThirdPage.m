//
//  NFThirdPage.m
//  NavigatorFramework
//
//  Created by chris on 16/1/2.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "NFThirdPage.h"

@interface NFThirdPage ()

@property(nonatomic, weak) IBOutlet UIButton        *backButton;

@end

@implementation NFThirdPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.backButton.layer.borderWidth = 1.0;
    self.backButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
}

- (IBAction)backToFirst:(id)sender{
    
    [self.navigator popFlow:nil];
}

- (IBAction)backToSecond:(id)sender {
    
    NSDictionary *params = @{@"thirdPage":@"yes"};
    [self.navigator backward:params];
}

@end

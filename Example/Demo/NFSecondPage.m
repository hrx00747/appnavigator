//
//  NFSecondPage.m
//  NavigatorFramework
//
//  Created by chris on 16/1/2.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "NFSecondPage.h"
#import "NFThirdPage.h"

@implementation Address

- (NSString*) description {
    
    return [NSString stringWithFormat:@"country:%@, province:%@, city:%@",self.country,self.province,self.city];
}

@end

@interface NFSecondPage ()

@property(nonatomic, weak) IBOutlet UIButton        *backButton;
@property(nonatomic, weak) IBOutlet UIButton        *toButton;

@end

@implementation NFSecondPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.backButton.layer.borderWidth = 1.0;
    self.backButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.toButton.layer.borderWidth = 1.0;
    self.toButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
}

- (void) pageDidForwardToMe{
    NSLog(@"name = %@, age = %ld, address = %@",self.name,(long)self.age,self.myAddress);
}

- (IBAction)backToFirst:(id)sender{
    
    NSDictionary* params = @{
                             @"test":@"hahaha"
                             };
    [self.navigator backward:params];
}


- (IBAction)toThird:(id)sender{
    
    [self.navigator forwardTo:@"app://NFThirdPage"];
}

@end

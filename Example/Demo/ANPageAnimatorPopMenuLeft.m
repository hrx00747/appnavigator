//
//  ANPageAnimatorPopMenuLeft.m
//  AppNavigator
//
//  Created by chris huang on 2018/12/28.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import "ANPageAnimatorPopMenuLeft.h"

#import "ANWeakify.h"

#define animationKey    @"menuAnimation"
#define kDefaultOverlap 100

@interface  ANPageAnimatorPopMenuLeft()<CAAnimationDelegate>

@property (copy) void (^callback)(void);
@property (copy) void (^animationFinished)(void);

@end

@implementation ANPageAnimatorPopMenuLeft

- (void) animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    if (self.animationFinished) {
        self.animationFinished();
    }
    self.animationFinished  = nil;
}

- (void) animateFrom:(UIViewController *)fromPage to:(UIViewController *)toPage callback:(void (^)(void))callback{
    
    self.callback = callback;
    UIView* from = fromPage.view;
    UIView* to = toPage.view;
    
    float duration = self.duration;
    
    CGFloat overlap = (self.overlapWidth - 0.000001) > 0 ? self.overlapWidth : kDefaultOverlap;self.callback = callback;
    
    CGRect oldTopInitFrame = from.frame;
    CGRect oldTopTargetFrame = from.frame;
    CGRect newTopTargetFrame = CGRectMake(0, 0, to.frame.size.width, to.frame.size.height);
    CGRect newTopInitFrame = CGRectOffset(newTopTargetFrame, oldTopInitFrame.size.width-overlap, 0);
    from.frame = oldTopInitFrame;
    to.frame = newTopInitFrame;
    
    CABasicAnimation*    fromAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    fromAnimation.duration = duration;
    fromAnimation.beginTime = 0;
    fromAnimation.valueFunction = [CAValueFunction functionWithName:kCAValueFunctionTranslateX];
    fromAnimation.timingFunction = self.timeFunction;
    fromAnimation.fromValue = [NSNumber numberWithFloat:0];
    fromAnimation.toValue = [NSNumber numberWithFloat:oldTopTargetFrame.origin.x-oldTopInitFrame.origin.x];
    fromAnimation.fillMode = kCAFillModeForwards;
    fromAnimation.removedOnCompletion = NO;
    [from.layer addAnimation:fromAnimation forKey:animationKey];
    
    
    CABasicAnimation*    toAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    toAnimation.delegate = self;
    toAnimation.duration = duration;
    toAnimation.beginTime = 0;
    toAnimation.valueFunction = [CAValueFunction functionWithName:kCAValueFunctionTranslateX];
    toAnimation.timingFunction = self.timeFunction;
    toAnimation.fromValue = [NSNumber numberWithFloat:0];
    toAnimation.toValue = [NSNumber numberWithFloat:newTopTargetFrame.origin.x-newTopInitFrame.origin.x];
    toAnimation.fillMode = kCAFillModeForwards;
    toAnimation.removedOnCompletion = NO;
    [to.layer addAnimation:toAnimation forKey:animationKey];
    
    @weakify(self);
    @weakify(from)
    @weakify(to)
    self.animationFinished = ^{
        @strongify(from)
        @strongify(to)
        @strongify(self);
        if(strong_self.callback){
            strong_self.callback();
        }
        [strong_to.layer removeAnimationForKey:animationKey];
        [strong_from.layer removeAnimationForKey:animationKey];
        strong_to.frame = newTopTargetFrame;
        strong_from.frame = oldTopInitFrame;
    };
}

@end

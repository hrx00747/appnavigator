//
//  MenuViewController.m
//  AppNavigator_Example
//
//  Created by chris huang on 2019/2/26.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "MenuViewController.h"
#import "ANNavigator.h"

#define SCREEN_WIDTH    [[UIScreen mainScreen] bounds].size.width

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
}

- (void) commonInit{
    
    CGFloat height = self.view.bounds.size.height;
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-100, height)];
    contentView.backgroundColor = [UIColor blueColor];
    [self.view addSubview:contentView];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 100, 50)];
    [button setTitle:@"Press" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:button];
    
    UIView *overlapView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-100, 0, 100, height)];
    [self.view addSubview:overlapView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [overlapView addGestureRecognizer:tap];
}

- (void) onButtonPressed:(UIButton*)button{
    
    [self.navigator forwardWithParams:@{@"url":@"app://NFSecondPage?name=hrx&age=33"} callback:nil];
}

- (void) onTap:(UIGestureRecognizer *)tap{
    
    [self.navigator backwardDepth:1 params:@{@"@animation":@"popmenu"}];
}



@end

 (function(){
  if ("undefined" == typeof com) {
  com = {};
  }
  
  if ("undefined" == typeof com.litb) {
  com.litb = {};
  }
  
  com.litb.Navigator = {
  
  setup: function(callback) {
  if (window.WebViewJavascriptBridge) { return callback(WebViewJavascriptBridge); }
  if (window.WVJBCallbacks) { return window.WVJBCallbacks.push(callback); }
  window.WVJBCallbacks = [callback];
  var WVJBIframe = document.createElement('iframe');
  WVJBIframe.style.display = 'none';
  WVJBIframe.src = 'https://__bridge_loaded__';
  document.documentElement.appendChild(WVJBIframe);
  setTimeout(function() {
             document.documentElement.removeChild(WVJBIframe)
             }, 0)
  },
  
  forward: function(url,params,callback) {
  
  var mergedParams = {};
  for(var key in params){
  mergedParams[key] = params[key];
  }
  mergedParams["url"] = url;
  window.WebViewJavascriptBridge.callHandler("forward", mergedParams, callback);
  },
  
  backward: function(params) {
  window.WebViewJavascriptBridge.callHandler("backward", params);
  },
  
  callback: function(params) {
  window.WebViewJavascriptBridge.callHandler("callback", params);
  },
  
  pushFlow: function() {
  window.WebViewJavascriptBridge.callHandler("pushFlow");
  },
  
  popFlow: function() {
  window.WebViewJavascriptBridge.callHandler("popFlow");
  }
  }
  
  com.litb.Navigator.setup(function(bridge){
                        
                           });
  })();

//
//  NFFirstPage.m
//  NavigatorFramework
//
//  Created by chris on 16/1/2.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "NFFirstPage.h"
#import "NFSecondPage.h"
#import "ANNavigationBarItem.h"
#import "ANNavigationBarView.h"

@interface NFFirstPage ()

@property(nonatomic, weak) IBOutlet  UIButton        *toButton;

@end

@implementation NFFirstPage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.toButton.layer.borderWidth = 1.0;
    self.toButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    ANNavigationBarItem *menu = [[ANNavigationBarItem alloc] initWithText:@"Menu" target:self selector:@selector(menu)];
    [self.navigationBarView addLeftItems:@[menu]];
    
    ANNavigationBarItem *growth = [[ANNavigationBarItem alloc] initWithImage:[UIImage imageNamed:@"icon_growth"] target:nil selector:nil];
    growth.width = 40;
    ANNavigationBarItem *message = [[ANNavigationBarItem alloc] initWithImage:[UIImage imageNamed:@"icon_message"] target:nil selector:nil];
    message.width = 35;
    [self.navigationBarView addRightItems:@[growth,message]];
    self.navigationBarView.titleView = self.toButton;
    
}

- (void) menu {
    
    [self.navigator forwardDepth:1 params:@{@"@animation":@"pushmenu"} callback:nil];
}

- (IBAction)buttonPressed:(id)sender{
    
    [self.navigator pushFlow];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"NFTestWebPage" ofType:@"html"];
//    NSString *url = [NSString stringWithFormat:@"file://%@",path];
//
//    [self.navigator forward:url callback:^(NSDictionary *params) {
//        NSLog(@"callback with params: %@",params);
//    }];
    
//    [self.navigator forwardTo:@"app://NFSecondPage?@animation=pushmenuleft&@temp=true&name=hrx&age=33" callback:^(NSDictionary *params) {
//        NSLog(@"callback with params: %@",params);
//    }];
    
    [self.navigator forwardWithParams:@{@"url":@"app://NFSecondPage?name=hrx&age=33&myAddress[Address].city=chengdu&myAddress[Address].province=sichuan",
                                        @"@navigationbar":@1,
                                        @"title":@"百度",
                                        @"navigationBackText":@"返回"
                                        } callback:nil];
    
//    Address *address = [[Address alloc] init];
//    address.country = @"China";
//    address.province = @"Sichuan";
//    address.city = @"Chengdu";
//    [self.navigator forwardWithParams:@{
//                                  @"url":@"app://NFSecondPage?name=hrx&age=33",
//                                  @"myAddress":address
//                                  } callback:^(NSDictionary *params) {
//                                      NSLog(@"callback with params: %@",params);
//                                  }];
}

@end

# AppNavigator

[![CI Status](https://img.shields.io/travis/huangruxue/AppNavigator.svg?style=flat)](https://travis-ci.org/huangruxue/AppNavigator)
[![Version](https://img.shields.io/cocoapods/v/AppNavigator.svg?style=flat)](https://cocoapods.org/pods/AppNavigator)
[![License](https://img.shields.io/cocoapods/l/AppNavigator.svg?style=flat)](https://cocoapods.org/pods/AppNavigator)
[![Platform](https://img.shields.io/cocoapods/p/AppNavigator.svg?style=flat)](https://cocoapods.org/pods/AppNavigator)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AppNavigator is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AppNavigator'
```

## Author

huangruxue, huangruxue@lightinthebox.com

## License

AppNavigator is available under the MIT license. See the LICENSE file for more info.

#
# Be sure to run `pod lib lint AppNavigator.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AppNavigator'
  s.version          = '0.3.3'
  s.summary          = 'App navigator or router'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
AppNavigator is a page navigator/router in iOS App
                       DESC

  s.homepage         = 'https://gitlab.com/hrx00747/appnavigator'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'hrx00747' => 'hrx00747@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/hrx00747/appnavigator.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'AppNavigator/Classes/**/*'
  s.resources = ['AppNavigator/Resources/**/*']
  
  # s.resource_bundles = {
  #   'AppNavigator' => ['AppNavigator/Assets/*.png']
  # }

  #s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'UIKit', 'Foundation'
   s.dependency 'Masonry','1.1.0'
   s.dependency 'WebViewJavascriptBridge', '6.0'

end

(function(){
  if ("undefined" == typeof com) {
  com = {};
  }
  
  if ("undefined" == typeof com.litb) {
  com.litb = {};
  }
  
  com.litb.Navigator = {
  
  setup: function() {
  var WVJBIframe = document.createElement('iframe');
  WVJBIframe.style.display = 'none';
  WVJBIframe.src = 'https://__bridge_loaded__';
  document.documentElement.appendChild(WVJBIframe);
  setTimeout(function() {
             document.documentElement.removeChild(WVJBIframe)
             }, 0)
  },
  
  forward: function(url, callback) {
  var params = {
  "url": url
  };
  window.WebViewJavascriptBridge.callHandler("forwrd", params, callback);
  },
  
  backward: function(params) {
  window.WebViewJavascriptBridge.callHandler("backword", params);
  },
  
  callback: function(params) {
  window.WebViewJavascriptBridge.callHandler("callback", params);
  },
  
  pushFlow: function() {
  window.WebViewJavascriptBridge.callHandler("pushFlow");
  },
  
  popFlow: function() {
  window.WebViewJavascriptBridge.callHandler("popFlow");
  }
  }
  
  com.litb.Navigator.setup();
})();

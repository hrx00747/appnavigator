//
//  ANPageAnimatorPopBottom.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANPageAnimatorPopBottom.h"
#import "ANWeakify.h"

#define animationKey @"popAnimation"

@interface ANPageAnimatorPopBottom()<CAAnimationDelegate>

@property (copy) void (^callback)(void);
@property (copy) void (^animationFinished)(void);

@end

@implementation ANPageAnimatorPopBottom


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (self.animationFinished) {
        self.animationFinished();
    }
    self.animationFinished  = nil;
}

- (void) animateFrom:(UIViewController *)fromPage to:(UIViewController *)toPage callback:(void (^)(void))callback{
    
    self.callback = callback;
    
    UIView* from = fromPage.view;
    UIView* to = toPage.view;
    
    float alphaRate = self.alphaRate;
    float duration = self.duration;
    
    CGRect oldTopInitFrame = from.frame;
    CGRect oldTopTargetFrame = CGRectOffset(oldTopInitFrame, 0, oldTopInitFrame.size.height);
    CGRect newTopTargetFrame = to.frame;
    CGRect newTopInitFrame = newTopTargetFrame;
    from.frame = oldTopInitFrame;
    to.frame = newTopInitFrame;
    
    UIView* mask = [[UIView alloc] initWithFrame:to.bounds];
    mask.backgroundColor = [UIColor blackColor];
    mask.alpha = alphaRate;
    [to addSubview:mask];
    
    CABasicAnimation*    fromAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    fromAnimation.delegate = self;
    fromAnimation.duration = duration;
    fromAnimation.beginTime = 0;
    fromAnimation.valueFunction = [CAValueFunction functionWithName:kCAValueFunctionTranslateY];
    fromAnimation.timingFunction = self.timeFunction;
    fromAnimation.fromValue = [NSNumber numberWithFloat:0];
    fromAnimation.toValue = [NSNumber numberWithFloat:oldTopTargetFrame.origin.y-oldTopInitFrame.origin.y];
    fromAnimation.fillMode = kCAFillModeForwards;
    fromAnimation.removedOnCompletion = NO;
    [from.layer addAnimation:fromAnimation forKey:animationKey];
    
    @weakify(self);
    @weakify(from)
    @weakify(to)
    self.animationFinished = ^{
        @strongify(from)
        @strongify(to)
        @strongify(self);
        if(strong_self.callback){
            strong_self.callback();
        }
        [strong_to.layer removeAnimationForKey:animationKey];
        [strong_from.layer removeAnimationForKey:animationKey];
        strong_to.frame = newTopTargetFrame;
        strong_from.frame = oldTopInitFrame;
    };
    
    @weakify(mask)
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        @strongify(mask)
        strong_mask.alpha = 0;
    } completion:^(BOOL finished) {
        @strongify(mask)
        [strong_mask removeFromSuperview];
    }];
}


@end

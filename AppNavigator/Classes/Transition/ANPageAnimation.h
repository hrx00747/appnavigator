//
//  ANPageAnimation.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/4.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ANPageAnimation <NSObject>


- (void) animateFrom:(UIViewController*)fromPage to:(UIViewController*)toPage callback:(void (^)(void))callback;


@end

//
//  ANPageAnimatorPushLeft.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANPageAnimator.h"
#import "ANPageAnimation.h"

@interface ANPageAnimatorPushLeft : ANPageAnimator<ANPageAnimation>

@end

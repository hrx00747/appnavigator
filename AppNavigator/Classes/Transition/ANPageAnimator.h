//
//  ANPageAnimator.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface ANPageAnimator : NSObject

@property (assign,nonatomic) float leaveRate;
@property (assign,nonatomic) float alphaRate;
@property (assign,nonatomic) float duration;
@property (strong,nonatomic) CAMediaTimingFunction* timeFunction;



@end

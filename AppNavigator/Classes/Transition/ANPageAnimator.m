//
//  ANPageAnimator.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANPageAnimator.h"

@implementation ANPageAnimator

-(instancetype) init {
    if(self=[super init]) {
        self.alphaRate = 0.4;
        self.leaveRate = 0.3;
        self.duration = 0.6;
        self.timeFunction = [CAMediaTimingFunction functionWithControlPoints:0 :0.6 :0.3 :1];
    }
    return self;
}


@end

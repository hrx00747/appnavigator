//
//  ANNavigationBarView.h
//  AppNavigator
//
//  Created by chris huang on 2019/1/8.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface ANNavigationBarView : UIView

@property(nonatomic, copy)      NSString        *title;
@property(nonatomic, strong)    UIView          *titleView;

@property(nonatomic, strong)    UIColor         *titleColor         UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)    UIColor         *textItemColor      UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)    UIColor         *imageItemColor     UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)    UIFont          *titleFont          UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong)    UIFont          *textItemFont       UI_APPEARANCE_SELECTOR;


- (void) addLeftItems:(NSArray*)items;

- (void) addRightItems:(NSArray*)items;


@end

NS_ASSUME_NONNULL_END

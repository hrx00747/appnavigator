//
//  ANNavigationBarItemView.h
//  AppNavigator
//
//  Created by chris huang on 2019/1/8.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANNavigationBarItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface ANNavigationBarItemView : UIControl

@property(nonatomic, strong)    UILabel         *label;
@property(nonatomic, strong)    UIImageView     *imageView;

@property(nonatomic, copy)      ClickHandler    handler;

@end

NS_ASSUME_NONNULL_END

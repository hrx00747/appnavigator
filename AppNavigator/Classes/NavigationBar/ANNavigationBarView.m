//
//  ANNavigationBarView.m
//  AppNavigator
//
//  Created by chris huang on 2019/1/8.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "ANNavigationBarView.h"
#import "ANNavigationBarItemView.h"
#import "ANNavigationBarItem.h"
#import "ANNavigator.h"
#import "ANUtils.h"

#define kHorizontalSpacing  10

@interface ANNavigationBarView ()

@property(nonatomic, strong)    UIView      *contentView;
@property(nonatomic, strong)    UILabel     *titleLabel;

@property(nonatomic, strong)    NSArray     *leftItemViews;
@property(nonatomic, strong)    NSArray     *rightItemViews;

@property(nonatomic, assign)    CGFloat     leftSpacing;
@property(nonatomic, assign)    CGFloat     rightSpacing;

@end

@implementation ANNavigationBarView

- (void) setTitle:(NSString *)title{
    
    _title = title;
    self.titleLabel.text = title;
}

- (void) setTitleView:(UIView *)titleView{
    
    _titleView = titleView;
    [self layoutIfNeeded];
}


- (instancetype) init{
    if(self=[super init]){
        [self commonInit];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        [self commonInit];
    }
    return self;
}

- (void) commonInit{
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.bounds)-kNavigationBarHeight, CGRectGetMaxX(self.bounds), kNavigationBarHeight)];
    [self addSubview:self.contentView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.titleLabel];
    
}

- (void) addLeftItems:(NSArray *)items {
    
    CGFloat itemHeight = self.contentView.bounds.size.height;
    CGFloat offsetX = 0;
    NSMutableArray *itemViewList = [[NSMutableArray alloc] init];
    for(ANNavigationBarItem *item in [self spacingItems:items]){
        if(!item.isSpacingFilling){
            ANNavigationBarItemView *itemView = [[ANNavigationBarItemView alloc] init];
            itemView.frame = CGRectMake(offsetX, 0, item.width, itemHeight);
            itemView.label.text = item.text;
            itemView.imageView.image = item.image;
            if(item.handler){
                itemView.handler = item.handler;
            }
            else if(item.target && item.selector){
                [itemView addTarget:item.target action:item.selector forControlEvents:UIControlEventTouchUpInside];
            }
            [itemViewList addObject:itemView];
            [self.contentView addSubview:itemView];
        }
        offsetX += item.width;
    }
    self.leftItemViews = [itemViewList copy];
    self.leftSpacing = offsetX;
}

- (void) addRightItems:(NSArray *)items{
    
    CGFloat itemHeight = self.contentView.bounds.size.height;
    CGFloat offsetX = CGRectGetMaxX(self.contentView.bounds);
    NSMutableArray *itemViewList = [[NSMutableArray alloc] init];
    for(ANNavigationBarItem *item in [self spacingItems:items]){
        if(!item.isSpacingFilling){
            ANNavigationBarItemView *itemView = [[ANNavigationBarItemView alloc] init];
            itemView.frame = CGRectMake(offsetX-item.width, 0, item.width, itemHeight);
            itemView.label.text = item.text;
            itemView.imageView.image = item.image;
            if(item.handler){
                itemView.handler = item.handler;
            }
            else if(item.target && item.selector){
                [itemView addTarget:item.target action:item.selector forControlEvents:UIControlEventTouchUpInside];
            }
            [itemViewList addObject:itemView];
            [self.contentView addSubview:itemView];
        }
        offsetX -= item.width;
    }
    self.rightItemViews = [itemViewList copy];
    self.rightSpacing = CGRectGetMaxX(self.contentView.bounds)-offsetX;
}

- (NSArray*) spacingItems:(NSArray*)items {
    
    NSMutableArray *itemList = [[NSMutableArray alloc] init];
    [itemList addObject:[self spacingItem]];
    [itemList addObjectsFromArray:items];
    [itemList addObject:[self spacingItem]];
    
    return [itemList copy];
}

- (ANNavigationBarItem*) spacingItem {
    
    ANNavigationBarItem *item = [[ANNavigationBarItem alloc] init];
    item.isSpacingFilling = YES;
    item.width = kHorizontalSpacing;
    
    return item;
}

- (void) layoutSubviews{
    
    [super layoutSubviews];
    
    NSMutableArray *itemViews = [[NSMutableArray alloc] init];
    [itemViews addObjectsFromArray:self.leftItemViews];
    [itemViews addObjectsFromArray:self.rightItemViews];
    for(ANNavigationBarItemView *itemView in itemViews){
        if(self.textItemColor){
            itemView.label.textColor = self.textItemColor;
        }
        if(self.imageItemColor){
            itemView.imageView.image = [itemView.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            itemView.imageView.tintColor = self.imageItemColor;
        }
        if(self.textItemFont){
            itemView.label.font = self.textItemFont;
        }
    }
    
    if(self.titleView){
        UIEdgeInsets insets = UIEdgeInsetsMake(0, self.leftSpacing, 0, self.rightSpacing);
        CGRect rect = UIEdgeInsetsInsetRect(self.contentView.bounds, insets);
        self.titleView.center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        [self.contentView addSubview:self.titleView];
        self.titleLabel.hidden = YES;
    }
    else{
        CGFloat spacing = MAX(self.leftSpacing, self.rightSpacing);
        UIEdgeInsets insets = UIEdgeInsetsMake(0, spacing, 0, spacing);
        self.titleLabel.frame = UIEdgeInsetsInsetRect(self.contentView.bounds, insets);;
        if(self.title){
            self.titleLabel.text = self.title;
        }
        if(self.titleColor){
            self.titleLabel.textColor = self.titleColor;
        }
        if(self.titleFont){
            self.titleLabel.font = self.titleFont;
        }
    }
    
}


@end

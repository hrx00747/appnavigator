//
//  ANNavigationBarItem.h
//  AppNavigator
//
//  Created by chris huang on 2019/1/8.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ClickHandler)(UIControl *control);

NS_ASSUME_NONNULL_BEGIN

@interface ANNavigationBarItem : NSObject

@property(nonatomic, copy)      NSString        *text;
@property(nonatomic, strong)    UIImage         *image;
@property(nonatomic, assign)    CGFloat         width;

@property(nonatomic, assign)    BOOL            isSpacingFilling;

@property(nonatomic)            SEL             selector;
@property(nonatomic, weak)      id              target;

@property(nonatomic, copy)      ClickHandler    handler;


- (instancetype) initWithText:(NSString*)text target:(nullable id)target selector:(nullable SEL)selector;

- (instancetype) initWithImage:(UIImage*)image target:(nullable id)target selector:(nullable SEL)selector;

- (instancetype) initWithText:(NSString*)text clickHandler:(ClickHandler)handler;

- (instancetype) initWithImage:(UIImage*)image clickHandler:(ClickHandler)handler;

@end

NS_ASSUME_NONNULL_END

//
//  ANNavigationBarItem.m
//  AppNavigator
//
//  Created by chris huang on 2019/1/8.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "ANNavigationBarItem.h"

#define kDefaultItemWidth           40

@implementation ANNavigationBarItem

- (instancetype) initWithText:(NSString *)text target:(id)target selector:(SEL)selector{
    if(self=[super init]){
        self.text = text;
        self.target = target;
        self.selector = selector;
        self.width = kDefaultItemWidth;
    }
    return self;
}

- (instancetype) initWithImage:(UIImage *)image target:(id)target selector:(SEL)selector{
    
    if(self=[super init]){
        self.image = image;
        self.target = target;
        self.selector = selector;
        self.width = kDefaultItemWidth;
    }
    return self;
}

- (instancetype) initWithText:(NSString *)text clickHandler:(ClickHandler)handler{
    
    if(self=[super init]){
        self.text = text;
        self.handler = handler;
        self.width = kDefaultItemWidth;
    }
    return self;
}

- (instancetype) initWithImage:(UIImage *)image clickHandler:(ClickHandler)handler{
    
    if(self=[super init]){
        self.image = image;
        self.handler = handler;
        self.width = kDefaultItemWidth;
    }
    return self;
}

@end

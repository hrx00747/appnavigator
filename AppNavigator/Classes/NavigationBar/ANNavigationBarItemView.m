//
//  ANNavigationBarItemView.m
//  AppNavigator
//
//  Created by chris huang on 2019/1/8.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "ANNavigationBarItemView.h"
#import "Masonry.h"

@implementation ANNavigationBarItemView

- (void) setHandler:(ClickHandler)handler{
    
    _handler = handler;
    [self addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (instancetype) init{
    if(self=[super init]){
        [self commonInit];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        [self commonInit];
    }
    return self;
}

- (void) commonInit{
    self.label = [[UILabel alloc] init];
    self.label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.label];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self);
    }];
    
    self.imageView = [[UIImageView alloc] init];
    [self addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self);
    }];
}

- (void) onClick:(UIControl*)control{
    
    if(self.handler){
        self.handler(self);
    }
}


@end

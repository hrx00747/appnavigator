//
//  ANPageInfo.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ANTargetInfo.h"

@class ANPage;

@interface ANPageHolder : NSObject

@property(nonatomic, strong)    ANPage          *page;
@property(nonatomic, copy)      NSString        *pageUrl;
@property(nonatomic, copy)      NSString        *pageName;
@property(nonatomic, strong)    NSDictionary    *pageParams;
@property(nonatomic, strong)    NSDictionary    *frameworkParams;

@property(nonatomic, copy)      ANPageCallback  pageCallback;


@end

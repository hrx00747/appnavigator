//
//  ANStack.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANStack.h"

@implementation ANStack{
    
    NSMutableArray      *_objects;
    NSMutableArray      *_tags;
}

- (instancetype) init{
    
    if(self=[super init]){
        _objects = [NSMutableArray array];
        _tags = [NSMutableArray array];
    }
    return self;
}


- (void) push:(id)object{
    
    [_objects addObject:object];
}

- (id) pop{
    
    id object = [_objects lastObject];
    id tag = [_tags lastObject];
    [_objects removeLastObject];
    if(object==tag){
        [_tags removeLastObject];
    }
    
    return object;
}

- (void) tag{
    
    id object = [_objects lastObject];
    [_tags addObject:object];
}

- (NSArray*) popToTag{
    
    id target = [_tags lastObject];
    return [self _popToTarget:target];
}

- (NSArray*) popToTarget:(id)target{
    
    return [self _popToTarget:target];
}

- (NSArray*) _popToTarget:(id)target{
    
    if(!target) return nil;
    
    NSMutableArray *popArray = [NSMutableArray array];
    id object = [self stackTop];
    while (object && object!=target) {
        [popArray insertObject:object atIndex:0];
        object = [self pop];
    }
    
    [self push:target];
    
    return [popArray copy];
}

- (id) stackTop{
    
    return [_objects lastObject];
}

- (id) stackItemWithDepth:(NSInteger)depth{
    
    NSInteger count = [self count];
    if(depth < count && depth >= 0){
        return _objects[count-depth-1];
    }
    else{
        return nil;
    }
}

- (NSInteger) count {
    
    return [_objects count];
}

- (BOOL) isEmpty {
    
    return [self count] == 0;
}

- (NSArray*) all {
    
    return [_objects copy];
}

@end

//
//  ANBridgeHelper.m
//  AppNavigator_Example
//
//  Created by chris huang on 2018/12/13.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import "ANBridgeHelper.h"
#import "ANNavigator.h"
#import "WebViewJavascriptBridge.h"

@interface ANBridgeHelper ()

@property(nonatomic, strong)    WebViewJavascriptBridge     *bridge;

@end

@implementation ANBridgeHelper


- (void) bindWebView:(UIWebView *)webView {
    
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:webView];
    [self registerNavigatorHandlers];
    
}

- (void) setWebViewDelegate:(id)webViewDelegate {
    
    if(self.bridge){
        [self.bridge setWebViewDelegate:webViewDelegate];
    }
}

- (void) registerNavigatorHandlers {
    
    [self.bridge registerHandler:@"forward" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *url = [self extractUrlFromData:data];
        if(url){
            void (^callback)(NSDictionary *params) = nil;
            if(responseCallback){
                callback = ^(NSDictionary *params){
                    responseCallback(params);
                };
            }
            [[ANNavigator main] forwardTo:url callback:callback];
        }
    }];
    
    [self.bridge registerHandler:@"backward" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSDictionary *params = [self extractParamsFromData:data];
        [[ANNavigator main] backward:params];
    }];
    
    [self.bridge registerHandler:@"callback" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSDictionary *params = [self extractParamsFromData:data];
        [[ANNavigator main] backward:params];
    }];
    
    [self.bridge registerHandler:@"pushFlow" handler:^(id data, WVJBResponseCallback responseCallback) {
        [[ANNavigator main] pushFlow];
    }];
    
    [self.bridge registerHandler:@"popFlow" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSDictionary *params = [self extractParamsFromData:data];
        [[ANNavigator main] popFlow:params];
    }];
    
    [self.bridge registerHandler:@"jump" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *url = [self extractUrlFromData:data];
        if(url){
            [[ANNavigator main] jumpTo:url];
        }
    }];
}

- (NSString*) extractUrlFromData:(id)data {
    
    NSString *urlString = nil;
    if([data isKindOfClass:[NSDictionary class]]){
        urlString = [data valueForKey:@"url"];
    }
    return urlString;
}

- (NSDictionary *) extractParamsFromData:(id)data {
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if([data isKindOfClass:[NSDictionary class]]){
        for(NSString *key in [data allKeys]){
            if(![key isEqualToString:@"url"]){
                params[key] = [data valueForKey:key];
            }
        }
    }
    return [params copy];
}


@end

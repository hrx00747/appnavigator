//
//  ANTargetInfo.m
//  AppNavigator
//
//  Created by chris huang on 2019/3/18.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import "ANTargetInfo.h"
#import "ANNavigator.h"
#import "ANUtils.h"

@implementation ANTargetInfo

+ (ANTargetInfo*) targetFromUrl:(NSString *)urlString {
    
    ANTargetInfo *target = [[ANTargetInfo alloc] init];
    target.urlString = urlString;
    if(urlString.length > 0){
        
        urlString = [ANUtils urlEncode:urlString];
        NSURL *url = [NSURL URLWithString:urlString];
        
        if(url){
            target.scheme = url.scheme;
            target.host = url.host;
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            NSMutableDictionary *atParams = [[NSMutableDictionary alloc] init];
            NSURLComponents *components = [NSURLComponents componentsWithString:urlString];
            for(NSURLQueryItem *item in [components queryItems]){
                NSString *key = [ANUtils urlDecode:item.name];
                NSString *value = [ANUtils urlDecode:item.value];
                if([key containsString:@"@"]){
                    atParams[key] = value;
                }
                else{
                    params[key] = value;
                }
            }
            
            target.params = [params copy];
            target.atParams = [atParams copy];
        }
    }
    
    return target;
}

+ (ANTargetInfo*) targetFromParams:(NSDictionary *)params{
    
    NSMutableDictionary *pageParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *atParams = [[NSMutableDictionary alloc] init];
    for(NSString *key in [params allKeys]){
        if([key containsString:@"@"]){
            [atParams setValue:params[key] forKey:key];
        }
        else{
            if(![key isEqualToString:@"url"]){
                [pageParams setValue:params[key] forKey:key];
            }
        }
    }
    
    ANTargetInfo *target = nil;
    NSString *urlString = [params objectForKey:@"url"];
    if(urlString){
        target = [ANTargetInfo targetFromUrl:urlString];
    }
    else{
        target = [[ANTargetInfo alloc] init];
    }
    [target mergeParams:[pageParams copy]];
    [target mergeAtParams:[atParams copy]];
    
    return target;
}

- (void) mergeParams:(NSDictionary *)params{
    
    NSMutableDictionary *mergeParams = [[NSMutableDictionary alloc] init];
    if(self.params){
        [mergeParams addEntriesFromDictionary:self.params];
    }
    if(params){
        [mergeParams addEntriesFromDictionary:params];
    }
    
    self.params = [mergeParams copy];
    
}

- (void) mergeAtParams:(NSDictionary *)params{
    
    NSMutableDictionary *mergeParams = [[NSMutableDictionary alloc] init];
    if(self.atParams){
        [mergeParams addEntriesFromDictionary:self.atParams];
    }
    if(params){
        [mergeParams addEntriesFromDictionary:params];
    }
    
    self.atParams = [mergeParams copy];
}

@end

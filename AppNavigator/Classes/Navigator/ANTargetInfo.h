//
//  ANTargetInfo.h
//  AppNavigator
//
//  Created by chris huang on 2019/3/18.
//  Copyright © 2019 huangruxue. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ANPageCallback)(NSDictionary *params);

NS_ASSUME_NONNULL_BEGIN

@interface ANTargetInfo : NSObject

@property(nonatomic, strong)    NSArray         *items;
@property(nonatomic, copy)      NSString        *urlString;
@property(nonatomic, copy)      NSString        *scheme;
@property(nonatomic, copy)      NSString        *host;
@property(nonatomic, strong)    NSDictionary    *params;
@property(nonatomic, strong)    NSDictionary    *atParams;

@property(nonatomic, copy)      ANPageCallback  callback;


+ (ANTargetInfo*) targetFromUrl:(NSString*)urlString;

+ (ANTargetInfo*) targetFromParams:(NSDictionary*)params;

- (void) mergeParams:(NSDictionary*)params;

- (void) mergeAtParams:(NSDictionary*)params;

@end

NS_ASSUME_NONNULL_END

//
//  ANNavigator.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANNavigator.h"
#import "ANTargetInfo.h"
#import "ANPageHolder.h"
#import "ANStack.h"
#import "ANPageAnimation.h"
#import "ANPageAnimator.h"
#import "ANPageAnimatorPushLeft.h"
#import "ANPageAnimatorPopRight.h"
#import "ANPageAnimatorPushTop.h"
#import "ANPageAnimatorPopBottom.h"
#import "ANPageLifeCircle.h"
#import "ANPageAware.h"
#import "ANWeakify.h"
#import "ANTabbarView.h"
#import "ANTabbarItem.h"
#import "ANWebPage.h"
#import "ANUtils.h"
#import "ANNavigationBarView.h"
#import "ANNavigationBarItem.h"


#define kFrameworkAnimation         @"@animation"           //动画参数
#define kFrameworkCache             @"@cache"               //是否被缓存
#define kFrameworkNavigationBar     @"@navigationbar"       //是否显示导航栏
#define kFrameworkIgnoreInBackward  @"@ignoreinbackward"    //backward的时候会被忽略
#define kFrameworkPrecondition      @"@precondition"        //是否有前置页面


typedef NS_ENUM(NSInteger, ANNavigatorType) {
    ANNavigatorTypeStack,
    ANNavigatorTypePlain
};

@interface ANNavigator()<ANTabbarViewDelegate>

@property(nonatomic, assign)    ANNavigatorType     navigatorType;
@property(nonatomic, assign)    ANNavigator         *subNavigator;
@property(nonatomic, strong)    ANStack             *pageStack;
@property(nonatomic, strong)    ANTabbarView        *tabbar;


@end

@implementation ANNavigator

+ (void) initialize {
    
    [[self pageAnimationRegistry] setObject:[ANPageAnimatorPushLeft class] forKey:@"pushleft"];
    [[self pageAnimationRegistry] setObject:[ANPageAnimatorPopRight class] forKey:@"popright"];
    [[self pageAnimationRegistry] setObject:[ANPageAnimatorPushTop class] forKey:@"pushtop"];
    [[self pageAnimationRegistry] setObject:[ANPageAnimatorPopBottom class] forKey:@"popbottom"];

}

+ (instancetype) main {
    static ANNavigator *instance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        instance = [[ANNavigator alloc] initWithType:ANNavigatorTypeStack];
    });
    return instance;
}

+ (void) registerSchemes:(NSArray *)schemes {
    
    [[self pageSchemeRegistry] addObjectsFromArray:schemes];
}

+ (void) registerPageAlias:(NSDictionary *)alias {
    
    [[self pageAliasRegistry] addEntriesFromDictionary:alias];
}

+ (void) registerPageAnimation:(id<ANPageAnimation>)animation forKey:(NSString *)key{
    
    [[self pageAnimationExtensionRegistry] setObject:animation forKey:key];
}

- (instancetype) initWithType:(ANNavigatorType)type {
    
    if(self=[super init]){
        [self commonInitWithType:type];
    }
    return self;
}

- (instancetype) init{
    NSAssert(NO, @"不能通过这个方法来生成Navigator");
    return nil;
}

- (void) commonInitWithType:(ANNavigatorType)type{
    self.pageStack = [[ANStack alloc] init];
    self.navigatorType = type;

    if(@available(iOS 11.0, *)){
        self.safeAreaInsets = [UIApplication sharedApplication].keyWindow.safeAreaInsets;
    }
    else{
        self.safeAreaInsets = UIEdgeInsetsMake(20, 0, 0, 0);
    }
    
    if(UIEdgeInsetsEqualToEdgeInsets(self.safeAreaInsets, UIEdgeInsetsZero)){
        self.safeAreaInsets = UIEdgeInsetsMake(20, 0, 0, 0);
    }
}


- (void) forwardTo:(NSString *)urlString{
    
    [self forwardTo:urlString callback:nil];
}

- (void) forwardTo:(NSString *)urlString callback:(ANPageCallback)callback{
    
    if(self.navigatorType == ANNavigatorTypePlain){
        [self.navigator forwardTo:urlString callback:callback];
        return;
    }
    
    ANTargetInfo *target = [ANTargetInfo targetFromUrl:urlString];
    target.callback = callback;
    [self _forward:target];
}

- (void) forwardWithParams:(NSDictionary *)params callback:(ANPageCallback)callback{
    
    if(self.navigatorType == ANNavigatorTypePlain){
        [self.navigator forwardWithParams:params callback:callback];
        return;
    }
    
    ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
    target.callback = callback;
    [self _forward:target];
}

- (void) forwardWithItems:(NSArray *)items params:(NSDictionary *)params callback:(ANPageCallback)callback{
    
    if(self.navigatorType == ANNavigatorTypeStack){
        
        ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
        target.items = items;
        target.callback = callback;
        [self _forward:target];
    }
    else{
        CGFloat height = self.safeAreaInsets.bottom + kTabbarHeight;
        CGRect frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-height, [UIScreen mainScreen].bounds.size.width, height);
        self.tabbar = [[ANTabbarView alloc] initWithFrame:frame items:items];
        self.tabbar.backgroundColor = [UIColor whiteColor];
        self.tabbar.delegate = self;
        [self.view addSubview:self.tabbar];
        ANTabbarItem *item = [items firstObject];
        [self jumpTo:item.url];
    }
}

- (void) forwardDepth:(NSInteger)depth params:(NSDictionary *)params callback:(ANPageCallback)callback{
    
    if(self.navigatorType == ANNavigatorTypePlain){
        [self.navigator forwardDepth:depth params:params callback:callback];
        return;
    }
    
    ANPageHolder *from = [self.pageStack stackTop];
    ANPageHolder *to = [self.pageStack stackItemWithDepth:depth];
    
    if(to){
        ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
        target.callback = callback;
        id<ANPageAnimation> animator = [self resolveAnimator:target forward:YES];
        [self transitionFrom:from to:to animator:animator forward:YES completion:^{
            [self addPageToTree:to.page];
        }];
    }
}

- (void) forwardTarget:(ANTargetInfo *)target{
    
    [self _forward:target];
}

- (void) _forward:(ANTargetInfo*)target{
    
    if([self needPrecondition:target])  return;
    
    ANPage *page = [self resolvePage:target];
    if(page == nil){
        return;
    }
    
    ANPageHolder *from = [self.pageStack stackTop];
    ANPageHolder *to = [self pageHolderWithPage:page target:target];
    [self addPageToTree:from.page];
    [self addPageToTree:to.page];
    
    id<ANPageAnimation> animator = [self resolveAnimator:target forward:YES];
    @weakify_self;
    [self transitionFrom:from to:to animator:animator forward:YES completion:^{
        @strongify_self;
        
        [self.pageStack push:to];
    }];
}

- (BOOL) needPrecondition:(ANTargetInfo*)target {
    
    BOOL need = NO;
    BOOL containPrecondition = [self.preconditionPages containsObject:target.host];
    if(target.atParams[kFrameworkPrecondition]){
        containPrecondition = [target.atParams[kFrameworkPrecondition] boolValue];
    }
    if(containPrecondition){
        if(self.preconditionTarget && self.preconditionBlock){
            
            need = self.preconditionBlock();
            if(need){
                @weakify_self;
                self.preconditionTarget.callback = ^(NSDictionary *params) {
                    @strongify_self;
                    BOOL success = [params[@"success"] boolValue];
                    if(success){
                        [self _forward:target];
                    }
                };
                [self _forward:self.preconditionTarget];
            }
        }
    }
    
    return need;
}

- (void) backward {
    
    [self backward:nil];
}

- (void) backward:(NSDictionary *)params{
    
    ANPageHolder *from = [self.pageStack stackTop];
    ANPageHolder *to = nil;
    NSInteger depth = 1;
    while (depth < [self.pageStack count]) {
        to = [self.pageStack stackItemWithDepth:depth];
        BOOL ignore = [to.frameworkParams[kFrameworkIgnoreInBackward] boolValue];
        if(ignore){
            depth ++;
        }
        else{
            break;
        }
    }
    
    if(to){
        [self addPageToTree:to.page];
        [self addPageToTree:from.page];
        ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
        id<ANPageAnimation> animator = [self resolveAnimator:target forward:NO];
        
        @weakify_self;
        [self transitionFrom:from to:to animator:animator forward:NO completion:^{
            @strongify_self;
            if(from.pageCallback){
                from.pageCallback(target.params);
            }
            ANPageHolder *pageHolder = [self.pageStack pop];
            [self removePageFromTree:pageHolder.page];
            
        }];
    }
}

- (void) backwardDepth:(NSInteger)depth params:(NSDictionary*)params{
    
    ANPageHolder *from = [self.pageStack stackItemWithDepth:depth];
    ANPageHolder *to = [self.pageStack stackTop];
    [self addPageToTree:to.page];
    
    if(from){
        ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
        id<ANPageAnimation> animator = [self resolveAnimator:target forward:YES];
        [self transitionFrom:from to:to animator:animator forward:YES completion:^{
            
        }];
    }
}

- (void) pushFlow{
    
    if(self.navigatorType == ANNavigatorTypePlain){
        [self.navigator pushFlow];
        return;
    }
    
    [self.pageStack tag];
}

- (void) popFlow:(NSDictionary *)params{
    
    ANPageHolder *from = [self.pageStack stackTop];
    NSArray *popPageHolders = [self.pageStack popToTag];
    ANPageHolder *to = [self.pageStack stackTop];
    
    [self addPageToTree:to.page];
    [self addPageToTree:from.page];
    
    ANTargetInfo *target = [ANTargetInfo targetFromParams:params];
    id<ANPageAnimation> animator = [self resolveAnimator:target forward:NO];
    
    @weakify_self;
    [self transitionFrom:from to:to animator:animator forward:NO completion:^{
        @strongify_self;
        if(from.pageCallback){
            from.pageCallback(target.params);
        }
        for(ANPageHolder *pageHolder in popPageHolders){
            [self removePageFromTree:pageHolder.page];
        }
        
    }];
}


- (void) callback:(NSDictionary *)params{
    
    ANPageHolder *pageHolder = [self.pageStack stackTop];
    if(pageHolder.pageCallback){
        pageHolder.pageCallback(params);
    }
    
}

- (void) jumpTo:(NSString*)urlString{
    
    if(self.navigatorType == ANNavigatorTypeStack){
        [self.subNavigator jumpTo:urlString];
        return;
    }
    
    ANTargetInfo *target = [ANTargetInfo targetFromUrl:urlString];
    
    ANPage *page = [self resolvePage:target];
    if(page == nil){
        return;
    }
    ANPageHolder *from = [self.pageStack stackTop];
    ANPageHolder *to = [self pageHolderWithPage:page target:target];
    
    [self addPageToTree:to.page];
    
    @weakify_self;
    [self transitionFrom:from to:to animator:nil forward:YES completion:^{
        @strongify_self;
        if(self.tabbar){
            [self.tabbar selectForUrl:urlString];
            [self.view bringSubviewToFront:self.tabbar];
        }
        [self removePageFromTree:from.page];
        [self.pageStack pop];
        [self.pageStack push:to];
    }];
}

- (ANPageHolder *) pageHolderWithPage:(ANPage*)page target:(ANTargetInfo*)target {
    
    ANPageHolder *pageHolder = [[ANPageHolder alloc] init];
    pageHolder.page = page;
    pageHolder.pageUrl = target.urlString;
    pageHolder.pageName = target.host;
    pageHolder.pageParams = target.params;
    pageHolder.frameworkParams = target.atParams;
    pageHolder.pageCallback = target.callback;
    return pageHolder;
}

- (void) autoWarePage:(ANPage*)page withParams:(NSDictionary*)params{
    
    NSLog(@"try autoware params to page : %@ ",NSStringFromClass([page class]));
    for (NSString *key in params.allKeys) {
        id value = params[key];
        NSLog(@"try autoware param key:%@ value:%@",key,value);
        
        [page warePageParamValue:value byKeyPath:key];
    }
}

- (NSString*) pageNameFromTarget:(ANTargetInfo*)target {
    
    NSArray *webSchemes = @[@"https",@"http",@"file"];
    NSString *pageName = nil;
    if([webSchemes containsObject:target.scheme]){
        pageName = @"ANWebPage";
    }
    else{
        pageName = target.host;
        NSString *alias = [ANNavigator pageAliasRegistry][pageName];
        if(alias.length > 0){
            pageName = alias;
        }
    }
    return pageName;
}

- (ANPage*) resolvePage:(ANTargetInfo*)target {
    
    ANPage *page = nil;
    NSString *pageName = nil;
    if(target.urlString.length > 0){
        pageName = [self pageNameFromTarget:target];
        BOOL pageCache = [target.atParams[kFrameworkCache] boolValue];
        if(pageCache){
            page = [[[self class] pageCacheRegistry] valueForKey:pageName];
        }
    }
    else if(target.items){
        ANNavigator *sub = [[ANNavigator alloc] initWithType:ANNavigatorTypePlain];
        sub.navigator = self;
        self.subNavigator = sub;
        [sub forwardWithItems:target.items params:target.params callback:target.callback];
        page = sub;
    }
    
    if(page == nil){
        Class clazz = NSClassFromString(pageName);
        if(clazz){
            page = [[clazz alloc] init];
            if([page respondsToSelector:@selector(pageInit)]){
                [((id<ANPageLifeCircle>)page) pageInit];
            }
            if([page respondsToSelector:@selector(setNavigator:)]){
                [((id<ANPageAware>)page) setNavigator:self];
            }
            if(self.tabbar && [page respondsToSelector:@selector(setTabbarView:)]){
                [((id<ANPageAware>)page) setTabbarView:self.tabbar];
            }
            if([page respondsToSelector:@selector(setPageUrl:)]){
                [((id<ANPageAware>)page) setPageUrl:target.urlString];
            }
        }
        if(page){
            BOOL hasNavigationBar = [target.atParams[kFrameworkNavigationBar] boolValue];;
            if(hasNavigationBar){
                page.navigationBarView = [self createNavigationBarView:target.params];
            }
        }
    }
    [self autoWarePage:page withParams:target.params];
    page.view.frame = self.view.bounds;
    if(page.navigationBarView){
        [page.view addSubview:page.navigationBarView];
    }
    return page;
}

- (ANNavigationBarView *) createNavigationBarView:(NSDictionary*)params{
    
    CGFloat height = self.safeAreaInsets.top + kNavigationBarHeight;
    ANNavigationBarView *navigationBar = [[ANNavigationBarView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMaxX(self.view.bounds), height)];
    if(params[@"title"]){
        navigationBar.title = params[@"title"];
    }
    NSMutableDictionary *callbackParams = [[NSMutableDictionary alloc] init];
    if(params[@"navigationBackAnimation"]){
        callbackParams[kFrameworkAnimation] = params[@"navigationBackAnimation"];
    }
    if(params[@"navigationBackText"]){
        @weakify_self;
        NSString *text = params[@"navigationBackText"];
        ANNavigationBarItem *backItem = [[ANNavigationBarItem alloc] initWithText:text clickHandler:^(UIControl *control) {
            @strongify_self;
            [self backward:[callbackParams copy]];
        }];
        [navigationBar addLeftItems:@[backItem]];
    }
    if(params[@"navigationBackImage"]){
        @weakify_self;
        NSString *imageName = params[@"navigationBackImage"];
        ANNavigationBarItem *backItem = [[ANNavigationBarItem alloc] initWithImage:[UIImage imageNamed:imageName] clickHandler:^(UIControl *control) {
            @strongify_self;
            [self backward:[callbackParams copy]];
        }];
        [navigationBar addLeftItems:@[backItem]];
    }
    return navigationBar;
}

- (id<ANPageAnimation>) resolveAnimator:(ANTargetInfo*)target forward:(BOOL)forward{
    
    id<ANPageAnimation> animator = nil;
    NSString *animationKey = kFrameworkAnimation;
    NSString *animation = [target.atParams[animationKey] lowercaseString];
    if(animation.length == 0){
        animation = @[@"popright",@"pushleft"][forward];
    }
    
    Class clazz = [ANNavigator pageAnimationRegistry][animation];
    if(clazz){
        animator = (id<ANPageAnimation>)[[clazz alloc] init];
    }
    
    if(animator == nil){
        animator = [ANNavigator pageAnimationExtensionRegistry][animation];
    }
    
    return animator;
}

- (void) transitionFrom:(ANPageHolder*)from to:(ANPageHolder*)to animator:(id<ANPageAnimation>)animator forward:(BOOL)forward completion:(void(^)(void))completion{
    
    ANPage *fromPage = from.page;
    ANPage *toPage = to.page;
    
    [self lifeCircleWillHappenFromPage:fromPage forward:forward];
    [self lifeCircleWillHappenToPage:toPage forward:forward];
    [fromPage beginAppearanceTransition:NO animated:YES];
    [toPage beginAppearanceTransition:YES animated:YES];
    
    void (^doneBlock)(void) = ^ {
        if(completion){
            completion();
        }
        
        [self lifeCircleDidHappenedFromPage:fromPage forward:forward];
        [self lifeCircleDidHappenedToPage:toPage forward:forward];
        [fromPage endAppearanceTransition];
        [toPage endAppearanceTransition];
    };
    
    if(animator){
        [animator animateFrom:fromPage to:toPage callback:^{
            doneBlock();
        }];
    }
    else{
        doneBlock();
    }
    
}

- (void) lifeCircleWillHappenFromPage:(ANPage*)page forward:(BOOL)forward{
    
    if([page respondsToSelector:@selector(pageWillBeHidden)]){
        [((id<ANPageLifeCircle>)page) pageWillBeHidden];
    }
    
    if(forward){
        if([page respondsToSelector:@selector(pageWillForwardFromMe)]){
            [((id<ANPageLifeCircle>)page) pageWillForwardFromMe];
        }
    }
    else{
        if([page respondsToSelector:@selector(pageWillBackwardFromMe)]){
            [((id<ANPageLifeCircle>)page) pageWillBackwardFromMe];
        }
    }
    
}

- (void) lifeCircleWillHappenToPage:(ANPage*)page forward:(BOOL)forward{
    
    if([page respondsToSelector:@selector(pageWillBeShown)]){
        [((id<ANPageLifeCircle>)page) pageWillBeShown];
    }
    
    if(forward){
        if([page respondsToSelector:@selector(pageWillForwardToMe)]){
            [((id<ANPageLifeCircle>)page) pageWillForwardToMe];
        }
    }
    else{
        if([page respondsToSelector:@selector(pageWillBackwardToMe)]){
            [((id<ANPageLifeCircle>)page) pageWillBackwardToMe];
        }
    }
}


- (void) lifeCircleDidHappenedFromPage:(ANPage*)page forward:(BOOL)forward{
    
    if([page respondsToSelector:@selector(pageDidHidden)]){
        [((id<ANPageLifeCircle>)page) pageDidHidden];
    }
    
    if(forward){
        if([page respondsToSelector:@selector(pageDidForwardFromMe)]){
            [((id<ANPageLifeCircle>)page) pageDidForwardFromMe];
        }
    }
    else{
        if([page respondsToSelector:@selector(pageDidBackwardFromMe)]){
            [((id<ANPageLifeCircle>)page) pageDidBackwardFromMe];
        }
    }
}

- (void) lifeCircleDidHappenedToPage:(ANPage*)page forward:(BOOL)forward {
    
    if([page respondsToSelector:@selector(pageDidShown)]){
        [((id<ANPageLifeCircle>)page) pageDidShown];
    }
    
    if(forward){
        if([page respondsToSelector:@selector(pageDidForwardToMe)]){
            [((id<ANPageLifeCircle>)page) pageDidForwardToMe];
        }
    }
    else{
        if([page respondsToSelector:@selector(pageDidBackwardToMe)]){
            [((id<ANPageLifeCircle>)page) pageDidBackwardToMe];
        }
    }
}

- (void) addPageToTree:(ANPage*)page {
    
    if(page){
        [self addChildViewController:page];
        [self.view addSubview:page.view];
    }
}

- (void) removePageFromTree:(ANPage*)page {
    
    if(page){
        [page.view removeFromSuperview];
        [page removeFromParentViewController];
    }
}

- (ANPage*) topPage {
    
    ANPageHolder *pageHolder = [self.pageStack stackTop];
    return pageHolder.page;
}

- (void) reload {
    
    if(self.subNavigator){
        [self.subNavigator reload];
    }
    
    for(ANPageHolder *pageHolder in [self.pageStack all]){
        if([pageHolder.page respondsToSelector:@selector(pageReload)]){
            [pageHolder.page pageReload];
        }
    }
}


NSMutableDictionary* ANNavigator_pageAnimationRegistry = nil;
+ (NSMutableDictionary *) pageAnimationRegistry {
    if(ANNavigator_pageAnimationRegistry == nil){
        ANNavigator_pageAnimationRegistry = [[NSMutableDictionary alloc] init];
    }
    return ANNavigator_pageAnimationRegistry;
}

NSMutableDictionary* ANNavigator_pageAnimationExtensionRegistry = nil;
+ (NSMutableDictionary *) pageAnimationExtensionRegistry {
    if(ANNavigator_pageAnimationExtensionRegistry == nil){
        ANNavigator_pageAnimationExtensionRegistry = [[NSMutableDictionary alloc] init];
    }
    return ANNavigator_pageAnimationExtensionRegistry;
}

NSMutableArray* ANNavigator_pageSchemeRegistry = nil;
+ (NSMutableArray*) pageSchemeRegistry {
    if(ANNavigator_pageSchemeRegistry == nil){
        ANNavigator_pageSchemeRegistry = [[NSMutableArray alloc] init];
    }
    return ANNavigator_pageSchemeRegistry;
}

NSMutableDictionary* ANNavigator_pageAliasRegistry = nil;
+ (NSMutableDictionary *) pageAliasRegistry {
    if(ANNavigator_pageAliasRegistry == nil){
        ANNavigator_pageAliasRegistry = [[NSMutableDictionary alloc] init];
    }
    return ANNavigator_pageAliasRegistry;
}

NSMutableDictionary* ANNavigator_pageCacheRegistry = nil;
+ (NSMutableDictionary *) pageCacheRegistry {
    if(ANNavigator_pageCacheRegistry == nil){
        ANNavigator_pageCacheRegistry = [[NSMutableDictionary alloc] init];
    }
    return ANNavigator_pageCacheRegistry;
}

NSMutableArray* ANNavigator_frameworkParamKeys = nil;
+ (NSMutableArray*) frameworkParamKeys {
    if(ANNavigator_frameworkParamKeys == nil){
        ANNavigator_frameworkParamKeys = [[NSMutableArray alloc] init];
    }
    return ANNavigator_frameworkParamKeys;
}

#pragma mark - ANTabbarViewDelegate
- (void) tabbarView:(ANTabbarView *)tabbarView didSelectWithItem:(ANTabbarItem *)item {
    
    ANPageHolder *pageHolder = [self.pageStack stackTop];
    if(![ANUtils pageUrl:pageHolder.pageUrl equalTo:item.url]){
        [self jumpTo:item.url];
    }
}

- (BOOL) shouldAutomaticallyForwardAppearanceMethods{
    
    return NO;
}

@end





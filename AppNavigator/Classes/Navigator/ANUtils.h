//
//  ANUtils.h
//  AppNavigator_Example
//
//  Created by chris huang on 2018/12/4.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ANUtils : NSObject

+ (BOOL) pageUrl:(NSString*)pageUrl1 equalTo:(NSString*)pageUrl2;

+ (NSNumber*) numberFromString:(NSString*)value signature:(const char*)type;

+ (NSString*) classNameFromSignature:(const char*)type;

+ (BOOL) isObjectFromSignature:(const char*)type;

+ (NSString*) capitalString:(NSString*)string;

+ (CGRect) cropRect:(CGRect)rect withInsets:(UIEdgeInsets)insets;

+ (UIEdgeInsets) mergeInsets:(UIEdgeInsets)one withInsets:(UIEdgeInsets)another;

+ (NSString*)urlEncode:(NSString*)string;

+ (NSString*)urlDecode:(NSString*)string;

@end

NS_ASSUME_NONNULL_END

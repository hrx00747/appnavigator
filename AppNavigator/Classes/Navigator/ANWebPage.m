//
//  ANWebPage.m
//  AppNavigator_Example
//
//  Created by chris huang on 2018/12/13.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import "ANWebPage.h"
#import "ANBridgeHelper.h"
#import "WebViewJavascriptBridge.h"
#import "ANNavigator.h"
#import "ANUtils.h"

#define kFilePrefix        @"file://"

@interface ANWebPage ()<UIWebViewDelegate>

@property(nonatomic, strong)    ANBridgeHelper      *helper;
@property(nonatomic, strong)    UIWebView           *webView;

@end

@implementation ANWebPage

- (ANBridgeHelper*) helper {
    if(!_helper){
        _helper = [[ANBridgeHelper alloc] init];
    }
    return _helper;
}

- (UIWebView*) webView {
    if(!_webView){
        CGRect frame = [UIScreen mainScreen].bounds;
        frame = [ANUtils cropRect:frame withInsets:self.navigator.safeAreaInsets];
        if(self.navigationBarView){
            frame = [ANUtils cropRect:frame withInsets:UIEdgeInsetsMake(kNavigationBarHeight, 0, 0, 0)];
        }
        _webView = [[UIWebView alloc] initWithFrame:frame];
    }
    return _webView;
}

- (void) viewDidLoad{
    
    [super viewDidLoad];
    [self.view addSubview:self.webView];
    [self.helper bindWebView:self.webView];
}


- (void) pageWillForwardToMe{
    
    [super pageWillForwardToMe];
    
    NSString *pageUrl = self.pageUrl;
    NSURL* url = nil;
    if ([pageUrl rangeOfString:kFilePrefix].location == 0) {
        NSString*   filePath = [pageUrl substringFromIndex:kFilePrefix.length];
        NSString* fileFolder = [filePath substringToIndex:
                                (filePath.length-filePath.lastPathComponent.length-1)];
        NSURL*       context = [NSURL fileURLWithPath:fileFolder];
        
        [self.webView loadHTMLString:[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil] baseURL:context];
    } else {
        url = [NSURL URLWithString:pageUrl];
        NSURLRequest* request = [NSURLRequest requestWithURL:url] ;
        [self.webView loadRequest:request];
    }
}




@end

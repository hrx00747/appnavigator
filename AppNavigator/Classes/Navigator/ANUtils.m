//
//  ANUtils.m
//  AppNavigator_Example
//
//  Created by chris huang on 2018/12/4.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import "ANUtils.h"

@implementation ANUtils

+ (BOOL) pageUrl:(NSString *)pageUrl1 equalTo:(NSString *)pageUrl2{
    
    NSURL *url1, *url2 = nil;
    if(pageUrl1){
        url1 = [NSURL URLWithString:[self urlEncode:pageUrl1]];
    }
    if(pageUrl2){
        url2 = [NSURL URLWithString:[self urlEncode:pageUrl2]];
    }
    
    return [url1.scheme isEqualToString:url2.scheme] && [url1.host isEqualToString:url2.host];
    
}

+(NSNumber*) numberFromString:(NSString*)value signature:(const char*)type {
    if (strcmp(type, "c") == 0) {
        char v = 0;
        if ([value hasPrefix:@"'"] && [value hasSuffix:@"'"] && value.length == 3) {
            v = [value characterAtIndex:1];
        } else {
            v = [value intValue];
        }
        if ([@"true" isEqualToString:value] || [@"YES" isEqualToString:value]) {
            v = 1;
        }
        if ([@"false" isEqualToString:value] || [@"NO" isEqualToString:value]) {
            v = 0;
        }
        return [NSNumber numberWithChar:v];
    }
    if (strcmp(type, "i") == 0) {
        return @([value intValue]);
    }
    if (strcmp(type, "I") == 0) {
        return @((unsigned int)[value intValue]);
    }
    if (strcmp(type, "s") == 0) {
        return @([value intValue]);
    }
    if (strcmp(type, "S") == 0) {
        return @([value intValue]);
    }
    if (strcmp(type, "l") == 0) {
        return @([value longLongValue]);
    }
    if (strcmp(type, "L") == 0) {
        return @([value longLongValue]);
    }
    if (strcmp(type, "f") == 0) {
        return @([value floatValue]);
    }
    if (strcmp(type, "d") == 0) {
        return @([value doubleValue]);
    }
    if (strcmp(type, "B") == 0) {
        return @([value boolValue]);
    }
    if (strcmp(type, "q") == 0) {
        return @([value longLongValue]);
    }
    if (strcmp(type, "Q") == 0){
        return @([value longLongValue]);
    }
    return nil;
}

+ (NSString*) classNameFromSignature:(const char *)type {
    
    NSString *className = nil;
    NSString *typeString = [NSString stringWithUTF8String:type];
    typeString = [typeString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"{}"]];
    NSArray *array = [typeString componentsSeparatedByString:@"="];
    if(array.count == 2){
        NSString *last = [array lastObject];
        if([last isEqualToString:@"#"]){
            className = [array firstObject];
        }
    }
    
    return className;
}

+ (BOOL) isObjectFromSignature:(const char *)type {
    
    return strcmp(type, "@") == 0;
}

+ (NSString*) capitalString:(NSString *)string {
    
    if(string.length == 0)  return string;
    
    NSString *capital = [string substringToIndex:1];
    NSString *remain = [string substringFromIndex:1];
    
    return [NSString stringWithFormat:@"%@%@",[capital uppercaseString],remain];
}

+ (CGRect) cropRect:(CGRect)rect withInsets:(UIEdgeInsets)insets {
    
    CGRect newRect;
    newRect.origin.x = rect.origin.x + insets.left;
    newRect.origin.y = rect.origin.y + insets.top;
    newRect.size.width = rect.size.width - insets.left - insets.right;
    newRect.size.height = rect.size.height - insets.top - insets.bottom;
    
    return newRect;
}

+ (UIEdgeInsets) mergeInsets:(UIEdgeInsets)one withInsets:(UIEdgeInsets)another{
    
    return UIEdgeInsetsMake(one.top+another.top, one.left+another.left, one.bottom+another.bottom, one.right+another.right);
}

+ (NSString*) urlEncode:(NSString *)string{
    
    return [string stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
}

+ (NSString*) urlDecode:(NSString *)string{
    
    return [string stringByRemovingPercentEncoding];
}

@end

//
//  ANBridgeHelper.h
//  AppNavigator_Example
//
//  Created by chris huang on 2018/12/13.
//  Copyright © 2018 huangruxue. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ANBridgeHelper : NSObject

- (void) bindWebView:(UIWebView*)webView;

- (void) setWebViewDelegate:(id)webViewDelegate;

@end

NS_ASSUME_NONNULL_END

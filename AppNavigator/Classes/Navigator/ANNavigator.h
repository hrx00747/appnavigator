//
//  ANNavigator.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ANPage.h"
#import "ANPageAnimation.h"
#import "ANTargetInfo.h"

#define kTabbarHeight           49
#define kNavigationBarHeight    44


@interface ANNavigator : ANPage

@property(nonatomic, assign)    UIEdgeInsets        safeAreaInsets;

+ (instancetype) main;

- (void) forwardTo:(NSString*)urlString;

- (void) forwardTo:(NSString*)urlString callback:(ANPageCallback)callback;

- (void) forwardWithParams:(NSDictionary*)params callback:(ANPageCallback)callback;

- (void) forwardWithItems:(NSArray*)items params:(NSDictionary*)params callback:(ANPageCallback)callback;

- (void) forwardDepth:(NSInteger)depth params:(NSDictionary*)params callback:(ANPageCallback)callback;

- (void) forwardTarget:(ANTargetInfo*)target;

- (void) backward;

- (void) backward:(NSDictionary*)params;

- (void) backwardDepth:(NSInteger)depth params:(NSDictionary*)params;

- (void) callback:(NSDictionary*)params;

- (void) pushFlow;

- (void) popFlow:(NSDictionary*)params;

- (void) jumpTo:(NSString*)urlString; //平时最好不要用这个方法，初始化的时候调用


@end


@interface ANNavigator(ANPage)

- (ANPage*) topPage;

@end

@interface ANNavigator(Register)

+(void) registerSchemes:(NSArray*)schemes;

+(void) registerPageAlias:(NSDictionary*)alias;

+(void) registerPageAnimation:(id<ANPageAnimation>)animation forKey:(NSString*)key;

@end

@interface ANNavigator(Reload)

- (void) reload;

@end


typedef BOOL(^PreconditionBlock)(void);

@interface ANNavigator ()

@property(nonatomic, strong)    ANTargetInfo        *preconditionTarget;
@property(nonatomic, copy)      PreconditionBlock   preconditionBlock;
@property(nonatomic, strong)    NSArray             *preconditionPages;

@end



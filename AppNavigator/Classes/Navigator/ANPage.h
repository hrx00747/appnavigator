//
//  ANPage.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANPageAware.h"
#import "ANPageLifeCircle.h"


@interface ANPage : UIViewController<ANPageAware, ANPageLifeCircle>

- (void) warePageParamValue:(id)value byKey:(NSString*)key;

- (void) warePageParamValue:(id)value byKeyPath:(NSString*)keyPath;

- (UIEdgeInsets) visibleInsets;

- (CGRect) visibleFrame;

- (void) pageReload;

@end

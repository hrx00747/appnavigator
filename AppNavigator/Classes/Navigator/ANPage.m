//
//  ANPage.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANPage.h"
#import "ANUtils.h"
#import "ANNavigator.h"

@interface ANPage ()

@end

@implementation ANPage

@synthesize tabbarView;
@synthesize navigationBarView;
@synthesize navigator;
@synthesize pageParams;
@synthesize frameworkParams;
@synthesize pageUrl;

- (void) warePageParamValue:(id)value byKeyPath:(NSString *)keyPath{
    
    NSArray *keys = [keyPath componentsSeparatedByString:@"."];
    NSMutableArray *targetList = [[NSMutableArray alloc] init];
    [targetList addObject:self];
    NSMutableArray *processedKeyList = [[NSMutableArray alloc] init];
    for(NSString *key in keys){
        NSInteger startIndex = [key rangeOfString:@"["].location;
        NSInteger endIndex = [key rangeOfString:@"]"].location;
        if(startIndex != NSNotFound && endIndex != NSNotFound){
            NSString *processedKey = [key substringToIndex:startIndex];
            [processedKeyList addObject:processedKey];
            NSObject *prevTarget = [targetList lastObject];
            if([prevTarget valueForKey:processedKey]){
                [targetList addObject:[prevTarget valueForKey:processedKey]];
            }
            else{
                NSString *targetName = [key substringWithRange:NSMakeRange(startIndex+1, endIndex-startIndex-1)];
                Class clazz = NSClassFromString(targetName);
                if(clazz){
                    NSObject *target = [[clazz alloc] init];
                    [targetList addObject:target];
                }
            }
        }
        else{
            [processedKeyList addObject:key];
        }
    }
    
    if(targetList.count == processedKeyList.count){
        id processedValue = value;
        for(NSInteger index=processedKeyList.count-1; index>=0; index--){
            NSString *processedKey = processedKeyList[index];
            NSObject *target = targetList[index];
            BOOL success = [self setValue:processedValue byKey:processedKey forTarget:target];
            if(success){
                processedValue = target;
            }
            else{
                break;
            }
        }
    }
}

- (BOOL) setValue:(id)value byKey:(NSString*)key forTarget:(NSObject*)target{
    
    BOOL success = YES;
    
    NSString* setterName = [NSString stringWithFormat:@"set%@:",[ANUtils capitalString:key]];
    SEL setterMethod = NSSelectorFromString(setterName);
    if([target respondsToSelector:setterMethod]) {
        NSMethodSignature* methodSign = [[target class] instanceMethodSignatureForSelector:setterMethod];
        const char * argType = [methodSign getArgumentTypeAtIndex:2];
        if([value isKindOfClass:[NSNumber class]]){
            NSNumber *number = (NSNumber*)value;
            [target setValue:number forKey:key];
        }
        else if([value isKindOfClass:[NSString class]]){
            NSString *string = (NSString*)value;
            NSNumber *number = [ANUtils numberFromString:string signature:argType];
            if(number){
                [target setValue:number forKey:key];
            }
            else{
                [target setValue:string forKey:key];
            }
        }
        else{
            if([ANUtils isObjectFromSignature:argType]){
                [target setValue:value forKey:key];
            }
            else{
                NSLog(@"skip param key:%@ value:%@",key,value);
                success = NO;
            }
        }
        
    } else {
        NSLog(@"skip param key:%@ value:%@",key,value);
        success = NO;
    }
    
    return success;
}

- (void) warePageParamValue:(id)value byKey:(NSString *)key{
    
    NSString* setterName = [NSString stringWithFormat:@"set%@:",[ANUtils capitalString:key]];
    SEL setterMethod = NSSelectorFromString(setterName);
    if([self respondsToSelector:setterMethod]) {
        NSMethodSignature* methodSign = [[self class] instanceMethodSignatureForSelector:setterMethod];
        const char * argType = [methodSign getArgumentTypeAtIndex:2];
        if([value isKindOfClass:[NSNumber class]]){
            NSNumber *number = (NSNumber*)value;
            [self setValue:number forKey:key];
        }
        else if([value isKindOfClass:[NSString class]]){
            NSString *string = (NSString*)value;
            NSNumber *number = [ANUtils numberFromString:string signature:argType];
            if(number){
                [self setValue:number forKey:key];
            }
            else{
                [self setValue:string forKey:key];
            }
        }
        else{
            if([ANUtils isObjectFromSignature:argType]){
                [self setValue:value forKey:key];
            }
            else{
                NSLog(@"skip param key:%@ value:%@",key,value);
            }
        }
        
    } else {
        NSLog(@"skip param key:%@ value:%@",key,value);
    }
}

- (UIEdgeInsets) visibleInsets {
    
    CGFloat top = 0;
    CGFloat bottom = 0;
    if(self.navigationBarView){
        top = kNavigationBarHeight;
    }
    if(self.tabbarView){
        bottom = kTabbarHeight;
    }
    UIEdgeInsets pageInsets = UIEdgeInsetsMake(top, 0, bottom, 0);
    return [ANUtils mergeInsets:pageInsets withInsets:self.navigator.safeAreaInsets];
}

- (CGRect) visibleFrame {
    
    return UIEdgeInsetsInsetRect(self.view.bounds, self.visibleInsets);
}

- (void) pageReload{
    
}

-(void) pageInit {
    [self log:@"pageInit"];
//    self.view.backgroundColor = [UIColor whiteColor];
}

-(void) pageDestroy {
    [self log:@"pageDestroy"];
}
/*!
 *  当页面即将向前切换到当前页面时调用
 */
-(void) pageWillForwardToMe {
    [self log:@"pageWillForwardToMe"];
}
/*!
 *  当页面已经向前切换到当前页面时调用
 */
-(void) pageDidForwardToMe{
    [self log:@"pageDidForwardToMe"];
}
/*!
 *  当页面即将向前离开当前页面时调用
 */
-(void) pageWillForwardFromMe{
    [self log:@"pageWillForwardFromMe"];
}
/*!
 *  当页面已经向前切换离开当前页面时调用
 */
-(void) pageDidForwardFromMe{
    [self log:@"pageDidForwardFromMe"];
}
/*!
 *  当页面即将向后回退到当前页面时调用
 */
-(void) pageWillBackwardToMe{
    [self log:@"pageWillBackwardToMe"];
}
/*!
 *  当页面已经向后回退到当前页面时调用
 */
-(void) pageDidBackwardToMe{
    [self log:@"pageDidBackwardToMe"];
}
/*!
 *  当页面即将后退离开当前页面时调用
 */
-(void) pageWillBackwardFromMe{
    [self log:@"pageWillBackwardFromMe"];
}

/*!
 *  当页面已经后退离开当前页面时调用
 */
-(void) pageDidBackwardFromMe{
    [self log:@"pageDidBackwardFromMe"];
}
/*!
 *  当页面即将展示时调用(包含页面前进和回退)
 */
-(void) pageWillBeShown{
    [self log:@"pageWillBeShown"];
}
/*!
 *  当页面已经展示时调用(包含页面前进和后退)
 */
-(void) pageDidShown{
    [self log:@"pageDidShown"];
}
/*!
 *  当页面即将隐藏(包含页面前进和后退)
 */
-(void) pageWillBeHidden{
    [self log:@"pageWillBeHidden"];
}

/*!
 *  当页面已经隐藏(包含前进和后退)
 */
-(void) pageDidHidden{
    [self log:@"pageDidHidden"];
}

- (void) log:(NSString*)text{
//    NSLog(@"%@ %@",text,NSStringFromClass([self class]));
}


@end

//
//  ANStack.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANStack : NSObject


/**
 压栈操作

 @param object 压栈的对象
 */
- (void) push:(id)object;


/**
 弹栈操作

 @return 弹栈的对象
 */
- (id) pop;


/**
 在当前栈的栈顶打上标签
 */
- (void) tag;


/**
 弹栈到最近一次标签

 @return 对象列表
 */
- (NSArray*) popToTag;


/**
 弹栈到一个目标对象

 @param target 目标对象
 @return 对象列表
 */
- (NSArray*) popToTarget:(id)target;


/**
 返回栈顶对象

 @return 栈顶对象
 */
- (id) stackTop;


/**
 返回从栈顶往下深度为deep的元素

 @param depth 深度: 0 表示栈顶元素
 @return 对象
 */
- (id) stackItemWithDepth:(NSInteger)depth;



/**
 返回堆栈元素个数

 @return 元素个数
 */
- (NSInteger) count;


/**
 判断堆栈是否为空

 @return 是否为空
 */
- (BOOL) isEmpty;



/**
 返回栈内所有元素

 @return 元素数组
 */
- (NSArray*) all;

@end

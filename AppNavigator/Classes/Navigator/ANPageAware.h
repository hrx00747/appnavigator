//
//  ANPageAware.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/3.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ANNavigator;
@class ANTabbarView;
@class ANNavigationBarView;

@protocol ANPageAware <NSObject>

/*
 *  页面导航控制器
 */
@property (weak,nonatomic) ANNavigator* navigator;

/**
 * 页面的底部的tabbar
 */
@property (weak, nonatomic) ANTabbarView* tabbarView;


/**
 * 页面顶部导航栏
 */
@property (strong, nonatomic) ANNavigationBarView* navigationBarView;

/*
 *  页面参数
 */
@property (strong,nonatomic) NSDictionary* pageParams;

/*
 *  框架参数
 */
@property (strong,nonatomic) NSDictionary* frameworkParams;


/*
 *  跳转时传入的url
 */
@property (nonatomic, copy) NSString    *pageUrl;



@end




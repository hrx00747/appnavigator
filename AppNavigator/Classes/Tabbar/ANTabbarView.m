//
//  ANTabbarView.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/6.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANTabbarView.h"
#import "ANTabbarItemView.h"
#import "ANTabbarItem.h"
#import "ANUtils.h"
#import "ANNavigator.h"

@interface ANTabbarView()

@property(nonatomic, strong)    NSArray     *items;
@property(nonatomic, strong)    NSArray     *itemViews;

@property(nonatomic, strong)    UIView      *contentView;

@end


@implementation ANTabbarView

- (instancetype) initWithFrame:(CGRect)frame items:(NSArray *)items {
    
    if(self=[super initWithFrame:frame]){
        self.items = items;
        [self commonInit];
    }
    
    return self;
}

- (void) commonInit {
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, kTabbarHeight)];
    [self addSubview:self.contentView];
    
    NSMutableArray *views = [[NSMutableArray alloc] init];
    CGFloat itemWidth = 0;
    CGFloat itemHeight = self.contentView.bounds.size.height;
    if(self.items.count > 0){
        itemWidth = self.contentView.bounds.size.width / self.items.count;
    }
    [self.items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect frame = CGRectMake(idx*itemWidth, 0, itemWidth, itemHeight);
        ANTabbarItemView *itemView = [[ANTabbarItemView alloc] initWithFrame:frame];
        itemView.tag = idx;
        [itemView configWithItem:obj];
        [self.contentView addSubview:itemView];
        [views addObject:itemView];
        [itemView addTarget:self action:@selector(onItemPressed:) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    self.itemViews = [views copy];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:line];
    
}

- (void) onItemPressed:(UIControl*)control {
    
    for(ANTabbarItemView *view in self.itemViews){
        view.selected = (view == control);
    }
    
    if(self.delegate){
        ANTabbarItem *item = self.items[control.tag];
        [self.delegate tabbarView:self didSelectWithItem:item];
    }
}


- (void) selectForUrl:(NSString *)urlString{
    
    [self.itemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ANTabbarItem *item = self.items[idx];
        ((ANTabbarItemView*)obj).selected = [ANUtils pageUrl:urlString equalTo:item.url];
        
    }];
}


- (void) addRedDotForKey:(NSString *)key{
    
    [self.itemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ANTabbarItem *item = self.items[idx];
        if([key isEqualToString:item.key]){
            [((ANTabbarItemView*)obj) clearBadge];
            [((ANTabbarItemView*)obj) addRedDot];
        }
    }];
}

- (void) clearRedDotForKey:(NSString *)key{
    
    [self.itemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ANTabbarItem *item = self.items[idx];
        if([key isEqualToString:item.key]){
            [((ANTabbarItemView*)obj) clearRedDot];
        }
    }];
}

- (void) addBadge:(NSInteger)badge forKey:(NSString *)key{
    
    [self.itemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ANTabbarItem *item = self.items[idx];
        if([key isEqualToString:item.key]){
            [((ANTabbarItemView*)obj) clearRedDot];
            [((ANTabbarItemView*)obj) addBadge:badge];
        }
    }];
}

- (void) clearBadgeForKey:(NSString *)key{
    
    [self.itemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ANTabbarItem *item = self.items[idx];
        if([key isEqualToString:item.key]){
            [((ANTabbarItemView*)obj) clearBadge];
        }
    }];
}

@end

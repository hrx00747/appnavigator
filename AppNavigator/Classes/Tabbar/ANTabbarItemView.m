//
//  ANTabbarItemView.m
//  AppNavigator
//
//  Created by chris huang on 2018/9/6.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import "ANTabbarItemView.h"
#import "Masonry.h"
#import "ANTabbarItem.h"

@interface ANTabbarItemView ()

@property(nonatomic, strong)    UIView      *reddotView;
@property(nonatomic, strong)    UIView      *badgeView;
@property(nonatomic, strong)    UILabel     *badgeLabel;

@property(nonatomic, strong)    ANTabbarItem *item;

@end

@implementation ANTabbarItemView

- (instancetype) initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        [self commonInit];
    }
    return self;
}

- (void) commonInit{
    
    self.iconView = [[UIImageView alloc] init];
    [self addSubview:self.iconView];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-5);
    }];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.iconView.mas_bottom).offset(5);
    }];
    
    self.reddotView = [[UIView alloc] init];
    self.reddotView.backgroundColor = [UIColor redColor];
    self.reddotView.layer.cornerRadius = 2.5;
    self.reddotView.layer.masksToBounds = YES;
    self.reddotView.hidden = YES;
    [self addSubview:self.reddotView];
    [self.reddotView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@5);
        make.height.equalTo(@5);
        make.left.equalTo(self.iconView.mas_right);
        make.bottom.equalTo(self.iconView.mas_top);
    }];
    
    self.badgeView = [[UIView alloc] init];
    self.badgeView.backgroundColor = [UIColor redColor];
    self.badgeView.layer.cornerRadius = 8.0;
    self.badgeView.layer.masksToBounds = YES;
    self.badgeView.hidden = YES;
    [self addSubview:self.badgeView];
    [self.badgeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.iconView.mas_right);
        make.centerY.equalTo(self.iconView.mas_top);
        make.height.equalTo(@16);
    }];
    
    self.badgeLabel = [[UILabel alloc] init];
    self.badgeLabel.font = [UIFont systemFontOfSize:12];
    self.badgeLabel.textColor = [UIColor whiteColor];
    [self.badgeView addSubview:self.badgeLabel];
    [self.badgeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.badgeView.mas_centerY);
        make.left.equalTo(self.badgeView.mas_left).offset(5);
        make.right.equalTo(self.badgeView.mas_right).offset(-5);
    }];
    
}

- (void) configWithItem:(ANTabbarItem *)item{
    
    self.item = item;
    
    self.iconView.image = [UIImage imageNamed:item.icon];
    self.iconView.highlightedImage = [UIImage imageNamed:item.selectedIcon];
    
    self.textLabel.text = item.title;
    if(item.attributes){
        self.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:item.title attributes:item.attributes];
    }
}

- (void) setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    self.iconView.highlighted = selected;
    
    NSDictionary *attributes = selected ? self.item.selectedAttributes : self.item.attributes;
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.item.title attributes:attributes];
    self.textLabel.attributedText = attributedText;
}

- (void) addRedDot {
    
    self.reddotView.hidden = NO;
}

- (void) clearRedDot {
    
    self.reddotView.hidden = YES;
}

- (void) addBadge:(NSInteger)badge{
    
    self.badgeLabel.text = [NSString stringWithFormat:@"%ld",(long)badge];
    self.badgeView.hidden = NO;
}

- (void) clearBadge{
    
    self.badgeView.hidden = YES;
}



@end

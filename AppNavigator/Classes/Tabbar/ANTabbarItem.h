//
//  ANTabbarItem.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/6.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ANTabbarItem : NSObject

@property(nonatomic, copy)      NSString        *key;
@property(nonatomic, copy)      NSString        *url;
@property(nonatomic, copy)      NSString        *title;
@property(nonatomic, copy)      NSString        *icon;
@property(nonatomic, copy)      NSString        *selectedIcon;
@property(nonatomic, strong)    NSDictionary    *attributes;
@property(nonatomic, strong)    NSDictionary    *selectedAttributes;

@end

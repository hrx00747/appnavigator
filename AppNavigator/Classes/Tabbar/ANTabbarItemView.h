//
//  ANTabbarItemView.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/6.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ANTabbarItem;

@interface ANTabbarItemView : UIControl

@property(nonatomic, strong)        UIImageView         *iconView;
@property(nonatomic, strong)        UILabel             *textLabel;

- (void) configWithItem:(ANTabbarItem*)item;

- (void) addRedDot;

- (void) clearRedDot;

- (void) addBadge:(NSInteger)badge;

- (void) clearBadge;

@end

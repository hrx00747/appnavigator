//
//  ANTabbarView.h
//  AppNavigator
//
//  Created by chris huang on 2018/9/6.
//  Copyright © 2018年 group4app. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ANTabbarItem;
@class ANTabbarView;

@protocol ANTabbarViewDelegate

@optional

- (void) tabbarView:(ANTabbarView*)tabbarView didSelectWithItem:(ANTabbarItem*)item;

@end

@interface ANTabbarView : UIView

@property(nonatomic, weak)  id<ANTabbarViewDelegate>        delegate;

- (instancetype) initWithFrame:(CGRect)frame items:(NSArray*)items;

- (void) selectForUrl:(NSString*)urlString;

- (void) addRedDotForKey:(NSString*)key;

- (void) clearRedDotForKey:(NSString*)key;

- (void) addBadge:(NSInteger)badge forKey:(NSString*)key;

- (void) clearBadgeForKey:(NSString*)key;

@end
